# Chatbot

# Clone
git clone https://AJConestoga@bitbucket.org/ajconestoga/assignment1.git

# Run locally
1. Navigate to cloned folder on local machine.
2. Run the command "npm install" to install dependencies.
3. Run the command "npm start" or "node index.js" to run the app on localhost.

# Deploy on Heroku
Run following commands
heroku login
heroku git:clone -a ajith-chatbot$ cd ajith-chatbot
git add .
git commit -am "make it better"
git push heroku master

# Contact me : ajithjoseph3210@conestogac.on.ca

# Note
The license used here is a short and simple permissive license with conditions only requiring preservation of copyright and license notices. This allows anyone to modify the terms too. This license suits best for this open source project.